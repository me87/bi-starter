import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { take } from 'rxjs/operators';
import {
  FieldValueChangeEvent,
  FormData,
  FormDataRequest,
} from 'src/app/bi-dynamic-form/bi-dynamic-form.model';
import { DynamicFormExampleServiceService } from 'src/app/shared/services/dynamic-form-example-service.service';

@Component({
  selector: 'dynamic-form-basic-example',
  templateUrl: './dynamic-form-basic-example.component.html',
  styleUrls: ['./dynamic-form-basic-example.component.scss'],
})
export class DynamicFormBasicExampleComponent implements OnInit {
  @Input() visible!: boolean;
  @Output() visibleChange = new EventEmitter<boolean>();

  formData!: FormData;

  formEditable = true;

  closeFormOptions!: any;
  submitFormOptions!: any;

  isFormValid!: boolean;

  constructor(private mockData: DynamicFormExampleServiceService) {}

  ngOnInit(): void {
    this.getFormData();

    // Definition for modal close button
    this.closeFormOptions = {
      text: 'Zatvori',
      icon: 'mdi mdi-close',
      type: 'normal',
      onClick: () => this.closeModal(),
    };

    // Definition for modal submit button
    this.submitFormOptions = {
      text: 'Sacuvaj',
      icon: 'mdi mdi-content-save',
      type: 'success',
      stylingMode: 'contained',
      onClick: () => this.closeModal(),
    };
  }

  getFormData() {
    this.mockData
      .getDefaultMock()
      .pipe(take(1))
      .subscribe((res) => {
        this.formData = res;
      });
  }

  formDataChange(event: FormDataRequest) {
    this.isFormValid = event.isFormValid || false;
    console.log('FORM FIELD CHAGE EVENT (RECEIVED): ', event);
  }

  closeModal() {
    this.visibleChange.emit(false);
  }
}
