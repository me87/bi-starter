import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { FormData } from 'src/app/bi-dynamic-form/bi-dynamic-form.model';
import { DynamicFormExampleServiceService } from 'src/app/shared/services/dynamic-form-example-service.service';

@Component({
  templateUrl: 'home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  // Default Form
  defaultForm!: FormData;

  defaultFormVisible = false;
  defaultFormEditable = false;

  closeDefaultFormOptions!: any;
  submitDefaultFormOptions!: any;

  // Header Form
  headerForm!: FormData;

  headerFormVisible = false;
  headerFormEditable = false;

  closeHeaderFormOptions!: any;
  submitHeaderFormOptions!: any;

  // Radio Form
  radioForm!: FormData;

  radioFormVisible = false;
  radioFormEditable = false;

  closeRadioFormOptions!: any;
  submitRadioFormOptions!: any;

  constructor(private mockData: DynamicFormExampleServiceService) {}

  ngOnInit() {

    // Default Form
    // Definition for default modal close button
    this.closeDefaultFormOptions = {
      text: 'Zatvori',
      icon: 'mdi mdi-close',
      type: 'normal',
      onClick: () => this.closeDefaultModal(),
    };

    // Definition for default modal submit button
    this.submitDefaultFormOptions = {
      text: 'Sacuvaj',
      icon: 'mdi mdi-content-save',
      type: 'success',
      stylingMode: 'contained',
      onClick: () => this.closeDefaultModal(),
    };

    // Header Form
    // Definition for header modal close button
    this.closeHeaderFormOptions = {
      text: 'Zatvori',
      icon: 'mdi mdi-close',
      type: 'normal',
      onClick: () => this.closeHeaderModal(),
    };

    // Definition for header modal submit button
    this.submitHeaderFormOptions = {
      text: 'Sacuvaj',
      icon: 'mdi mdi-content-save',
      type: 'success',
      stylingMode: 'contained',
      onClick: () => this.closeHeaderModal(),
    };

    // Radio Form
    // Definition for radio modal close button
    this.closeRadioFormOptions = {
      text: 'Zatvori',
      icon: 'mdi mdi-close',
      type: 'normal',
      onClick: () => this.closeRadioModal(),
    };

    // Definition for radio modal submit button
    this.submitRadioFormOptions = {
      text: 'Sacuvaj',
      icon: 'mdi mdi-content-save',
      type: 'success',
      stylingMode: 'contained',
      onClick: () => this.closeRadioModal(),
    };
  }

  // Open default modal
  openDefaultModal() {
    this.defaultFormVisible = true;
  }

  closeDefaultModal() {
    this.defaultFormVisible = false;
  }

  // Open header modal
  openHeaderModal() {
    this.headerFormVisible = true;
  }

  closeHeaderModal() {
    this.headerFormVisible = false;
  }

  // Open radio modal
  openRadioModal() {
    this.mockData
      .getRadioMock()
      .pipe(take(1))
      .subscribe(res => {
        this.radioForm = res;
        this.radioFormVisible = true;
      });
  }

  closeRadioModal() {
    this.radioFormVisible = false;
  }

  defaultFormUpdated(event:any) {
    // debugger
    // this.defaultForm.attributes.map(item => {
    //   if (item.name === event.fieldName) {
    //     item.value = event.newValue
    //   }
    //   return item;
    // })
    // this.defaultForm = { ...this.defaultForm }
    console.log('DEFAULT FORM: ', this.defaultForm);
    console.log('CHANGE EVENT: ', event);
  }

  headerFormUpdated() {
    console.log('HEADER FORM: ', this.headerForm);
  }

  radioFormUpdated() {
    console.log('HEADER FORM: ', this.radioForm);
  }
}
