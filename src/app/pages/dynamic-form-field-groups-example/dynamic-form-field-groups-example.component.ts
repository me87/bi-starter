import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { take } from 'rxjs/operators';
import { FieldValueChangeEvent, FormData, FormDataRequest } from 'src/app/bi-dynamic-form/bi-dynamic-form.model';
import { DynamicFormExampleServiceService } from 'src/app/shared/services/dynamic-form-example-service.service';

@Component({
  selector: 'dynamic-form-field-groups-example',
  templateUrl: './dynamic-form-field-groups-example.component.html',
  styleUrls: ['./dynamic-form-field-groups-example.component.scss']
})
export class DynamicFormFieldGroupsExampleComponent implements OnInit {

  @Input() visible!: boolean;
  @Output() visibleChange = new EventEmitter<boolean>();

  formData!: FormData;

  formEditable = false;

  closeFormOptions!: any;
  submitFormOptions!: any;

  constructor(private mockData: DynamicFormExampleServiceService) {}

  ngOnInit(): void {
    this.getFormData();

    // Definition for modal close button
    this.closeFormOptions = {
      text: 'Zatvori',
      icon: 'mdi mdi-close',
      type: 'normal',
      onClick: () => this.closeModal(),
    };

    // Definition for modal submit button
    this.submitFormOptions = {
      text: 'Sacuvaj',
      icon: 'mdi mdi-content-save',
      type: 'success',
      stylingMode: 'contained',
      onClick: () => this.closeModal(),
    };
  }

  getFormData() {
    this.mockData
      .getHeaderMock()
      .pipe(take(1))
      .subscribe((res) => {
        this.formData = res;
      });
  }

  formDataChange(event: FormDataRequest) {
    console.log('FORM FIELD CHAGE EVENT (RECEIVED): ', event);
  }

  closeModal() {
    this.visibleChange.emit(false);
  }

}
