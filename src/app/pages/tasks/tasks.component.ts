import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import 'devextreme/data/odata/store';
import { tasksData } from './task.mock';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  templateUrl: 'tasks.component.html',
})
export class TasksComponent {
  dataSource: any;
  priority: any[];
  childTasksDataSource: any;
  detailsDataSource: any;
  formTableDataSource: any;
  // tree list datasource
  treeListDataSource: any;

  filter = {
    process: '',
    product: '',
    oj: '',
    processid: '',
    statuses: [],
    client: null,
    fl_1: '',
    fl_2: '',
    hireDate: new Date(2012, 4, 13),
  };

  form = {
    jmbg: '12345678904',
    name: 'Petar',
    surname: 'Petrovic',
    address: 'Sofkina 013, 17501 - Vranje',
    residentStatus: '1',
    residentStatusLabel: 'Rezident',
    limit: 0,
    limitLabel: 'MasterCard Standard u okviru paketa tekuceg racuna',
    validDate: new Date(2022, 4, 13),
    cardNumber: '233457788532212',
    accountNumber: '0987654',
    currentLimit: 300,
    totalIncome: 0,
    analysisRequired: false,
    note: '',
  };

  hireDateOptions = {
    disabled: true,
  };

  processOptions = ['Process 1', 'Process 2', 'Process 3'];
  productOptions = ['Product 1', 'Product 2', 'Product 3'];
  statusOptions = [
    '10 Kreiranje ponude u toku',
    '20 Kreirana ponuda',
    '30 Zahtev kreiran',
    '35 AML obrada',
    '40 Automatika',
    '50 KA junior',
  ];
  clientOptions = ['PL', 'FL'];
  statusesDataOptions = [
    'Not Started',
    'Need Assistance',
    'In Progress',
    'Deferred',
    'Completed',
  ];

  closeButtonOptions: any;

  closeFormOptions: any;

  submitFormOptions: any;

  detailsVisible = false;

  formVisible = false;

  constructor(http: HttpClient) {
    // TREE LIST DATASOURCE
    this.treeListDataSource = {
      load: function (loadOptions: any) {
        return (
          http
            .get(
              'https://js.devexpress.com/Demos/Mvc/api/treeListData?parentIds=' +
                (loadOptions.parentIds || '')
            )
            // THIS IS HERE TO PREVENT HTTP IF NO PARENT
            .pipe(catchError((err) => of([])))
            .toPromise()
        );
      },
      key: 'id',
    };

    this.dataSource = {
      store: {
        type: 'odata',
        key: 'Task_ID',
        url: 'https://js.devexpress.com/Demos/DevAV/odata/Tasks',
      },
      expand: 'ResponsibleEmployee',
      select: [
        'Task_ID',
        'Task_Subject',
        'Task_Start_Date',
        'Task_Due_Date',
        'Task_Status',
        'Task_Priority',
        'Task_Completion',
        'ResponsibleEmployee/Employee_Full_Name',
      ],
    };

    this.priority = [
      { name: 'High', value: 4 },
      { name: 'Urgent', value: 3 },
      { name: 'Normal', value: 2 },
      { name: 'Low', value: 1 },
    ];

    this.detailsDataSource = [
      {
        label: 'Datum zahteva',
        value: '08.11.2019',
      },
      {
        label: 'OJ zahteva',
        value: '005420',
      },
      {
        label: 'Sifra radnika zahteva',
        value: '978',
      },
      {
        label: 'Sifra radnika promene',
        value: '556',
      },
      {
        label: 'datum poslednje promene',
        value: '-',
      },
    ];

    this.formTableDataSource = [
      {
        id: '123456',
        value: 10000.56,
        currency: 'EUR',
        date: '08.11.2019',
        incomeType: 'Some kind of income type',
      },
      {
        id: '123456',
        value: 10000.56,
        currency: 'EUR',
        date: '08.11.2019',
        incomeType: 'Some kind of income type',
      },
      {
        id: '123456',
        value: 10000.56,
        currency: 'EUR',
        date: '08.11.2019',
        incomeType: 'Some kind of income type',
      },
    ];

    this.childTasksDataSource = this.getTasksData();

    this.viewDetails = this.viewDetails.bind(this);

    this.closeButtonOptions = {
      text: 'Zatvori',
      icon: 'mdi mdi-close',
      type: 'normal',
      onClick: () => (this.detailsVisible = false),
    };

    this.closeFormOptions = {
      text: 'Zatvori',
      icon: 'mdi mdi-close',
      type: 'normal',
      onClick: () => (this.formVisible = false),
    };

    this.submitFormOptions = {
      text: 'Sacuvaj',
      icon: 'mdi mdi-content-save',
      type: 'success',
      stylingMode: 'contained',
      onClick: () => (this.formVisible = false),
    };
  }

  // TREE LIST LIMIT TO 1 LEVEL DEPTH
  // IT IS HANDLED WITH CSS SO USE IT ONLY IF YOU WANT TO REMOVE IT FROM DOM
  cellPrepared(e: any) {
    if (e.rowType === 'data') {
      if (e.row.level >= 1) {
        let addLink = e.cellElement.querySelector('.dx-treelist-collapsed');

        if (addLink) {
          addLink.remove();
        }
      }
    }
  }

  viewDetails(e: any) {
    console.log(e.row);
    this.detailsVisible = true;
    e.event.preventDefault();
  }

  viewForm() {
    this.formVisible = true;
  }

  selectionChanged(e: any) {
    alert('aaa');
    e.component.collapseAll(-1);
    e.component.expandRow(e.currentSelectedRowKeys[0]);
  }

  getTasksData(): any[] {
    return tasksData;
  }
}
