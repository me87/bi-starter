import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  FormDataResponse,
  FormData,
} from 'src/app/bi-dynamic-form/bi-dynamic-form.model';

@Injectable({
  providedIn: 'root',
})
export class DynamicFormExampleServiceService {
  constructor(private httpClient: HttpClient) {}

  getDefaultMock(): Observable<FormData> {
    return this.httpClient
      .get<FormDataResponse>('/assets/mock/default.mock.json')
      .pipe(map((res) => res.data));
  }

  getHeaderMock(): Observable<FormData> {
    return this.httpClient
      .get<FormDataResponse>('/assets/mock/header.mock.json')
      .pipe(map((res) => res.data));
  }

  getRadioMock(): Observable<FormData> {
    return this.httpClient
      .get<FormDataResponse>('/assets/mock/list.mock.json')
      .pipe(map((res) => res.data));
  }
}
