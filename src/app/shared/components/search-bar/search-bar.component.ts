import { Component, NgModule, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DxListModule } from 'devextreme-angular/ui/list';
import { DxContextMenuModule } from 'devextreme-angular/ui/context-menu';
import { IUser } from '../../services/auth.service';
import { navigation } from 'src/app/app-navigation';
import { DxAutocompleteModule } from 'devextreme-angular';

@Component({
  selector: 'app-search-bar',
  templateUrl: 'search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})

export class SearchBarComponent {
  app = "";
  applications = this.getApplicationLabels();

  constructor() {
  }

  getApplicationLabels() {
    const applications: any[] = navigation.filter(item => item.items).map(item => item.items);
    const labels = applications.reduce((acc, val) => acc.concat(val), []).map((item: any) => item.text)
    return labels;
  }

  search(event: string) {
    console.log('SEARCH RESULT', event);
    setTimeout(() => {
      this.app = '';
    }, 500);
  }
}

@NgModule({
  imports: [
    DxListModule,
    DxContextMenuModule,
    DxAutocompleteModule,
    CommonModule
  ],
  declarations: [ SearchBarComponent ],
  exports: [ SearchBarComponent ]
})
export class SearchBarModule { }
