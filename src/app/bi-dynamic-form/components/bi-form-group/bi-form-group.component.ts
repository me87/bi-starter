import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  Input,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { FieldAttributes } from '../../bi-dynamic-form.model';

@Component({
  selector: 'bi-form-group',
  templateUrl: './bi-form-group.component.html',
  styleUrls: ['./bi-form-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BiFormGroupComponent implements OnInit {
  @Input() dataSource: FieldAttributes[] = [];

  @ContentChild('group', { static: false }) groupTemplateRef!: TemplateRef<any>;

  constructor() {}

  ngOnInit(): void {}

  getHeaderValue(fields: FieldAttributes[]) {
    const header = fields.find(item => item.type === 'Header');
    return header ? header.label : '';
  }
}
