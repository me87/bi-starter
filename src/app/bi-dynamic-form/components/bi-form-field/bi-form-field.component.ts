import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldAttributes } from '../../bi-dynamic-form.model';

@Component({
  selector: 'bi-form-field',
  templateUrl: './bi-form-field.component.html',
  styleUrls: ['./bi-form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BiFormFieldComponent implements OnInit {

  @Input() config!: FieldAttributes;
  @Input() formGroup!: FormGroup;

  @Output() fieldChange = new EventEmitter<FieldAttributes>();
  @Output() lookupChange = new EventEmitter<any>();

  get disabled() {
    const control = this.formGroup.get(this.config.name)
    return control ? control.disabled : true;
  }

  constructor() { }

  ngOnInit(): void {
  }

  handleValueChange(field: FieldAttributes) {
    this.fieldChange.emit(field);
  }

  handleLookupChange(value: any) {
    this.lookupChange.emit(value);
  }

}
