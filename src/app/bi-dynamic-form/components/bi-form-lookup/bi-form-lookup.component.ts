import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, take } from 'rxjs/operators';
import { DataGridConfig, FieldAttributes } from '../../bi-dynamic-form.model';
import { LookupService } from '../../services/lookup.service';
import { BiFormFieldComponent } from '../bi-form-field/bi-form-field.component';

@Component({
  selector: 'bi-form-lookup',
  templateUrl: './bi-form-lookup.component.html',
  styleUrls: ['./bi-form-lookup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BiFormLookupComponent extends BiFormFieldComponent implements OnDestroy {
  @Input() visible!: boolean;
  @Input() currentValue!: string;
  @Input() attributes!: FieldAttributes;
  @Input() options!: DataGridConfig;

  @Output() visibleChange = new EventEmitter<any>();
  @Output() searchChange = new EventEmitter<string>();

  closeFormOptions!: any;
  submitFormOptions!: any;

  selectedRow!: any;

  private subject: Subject<string> = new Subject();

  constructor(
    private lookupService: LookupService,
    private cd: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit(): void {

    if (this.currentValue) {
      console.log('VALUE: ', this.currentValue);
      this.getSearchResults(this.currentValue);
    }

    // Subscribe for searchtext value changes
    this.subject.pipe(debounceTime(500)).subscribe((searchText: string) => {
      this.getSearchResults(searchText);
    });

    // Definition for modal close button
    this.closeFormOptions = {
      text: 'Zatvori',
      icon: 'mdi mdi-close',
      type: 'normal',
      onClick: () => this.closeModal(),
    };

    // Definition for modal submit button
    this.submitFormOptions = {
      text: 'Izaberi',
      icon: 'mdi mdi-content-save',
      type: 'success',
      stylingMode: 'contained',
      onClick: () => this.closeModal(true),
    };
  }

  getSearchResults(searchText: string) {
    this.searchChange.emit(searchText);
  }

  search(event: any) {
    this.subject.next(event.target.value);
  }

  selectRow(row: any) {
    this.selectedRow = row;
    console.log('ROW', this.selectedRow);
  }

  closeModal(isSubmit: boolean = false) {
    this.visibleChange.emit(isSubmit ? this.selectedRow : null);
  }

  ngOnDestroy(): void {
      this.subject.unsubscribe();
  }
}
