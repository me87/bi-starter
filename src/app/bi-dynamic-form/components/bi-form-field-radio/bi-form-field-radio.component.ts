import { Component, OnInit } from '@angular/core';
import { BiFormFieldComponent } from '../bi-form-field/bi-form-field.component';

@Component({
  selector: 'bi-form-field-radio',
  templateUrl: './bi-form-field-radio.component.html',
  styleUrls: ['./bi-form-field-radio.component.scss']
})
export class BiFormFieldRadioComponent extends BiFormFieldComponent {

}
