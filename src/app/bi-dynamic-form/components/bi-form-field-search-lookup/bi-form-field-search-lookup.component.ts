import { AfterContentInit, AfterViewInit, ChangeDetectorRef, Component, ContentChild, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DataGridConfig } from '../../bi-dynamic-form.model';
import { BiFormFieldComponent } from '../bi-form-field/bi-form-field.component';
import { BiFormLookupComponent } from '../bi-form-lookup/bi-form-lookup.component';

@Component({
  selector: 'bi-form-field-search-lookup',
  templateUrl: './bi-form-field-search-lookup.component.html',
  styleUrls: ['./bi-form-field-search-lookup.component.scss']
})
export class BiFormFieldSearchLookupComponent extends BiFormFieldComponent {

  @Input() options!: DataGridConfig;
  @Output() searchChange = new EventEmitter<string>();

  openLookup: boolean = false;

  get currentValue() {
    return this.formGroup.get(this.config.name)?.value;
  }

  search(searchText: string) {
    this.searchChange.emit(searchText);
  }

  toggleLookup() {
    this.openLookup = !this.openLookup;
  }

  keyDownEvent(event: KeyboardEvent) {
    if (event.key === 'F3') {
      this.toggleLookup();
    }
  }

  valueSelected(event: any) {
    console.log('SELECTED VALUE: ', event);
    this.toggleLookup();
    event && this.handleLookupChange(event);
  }

}
