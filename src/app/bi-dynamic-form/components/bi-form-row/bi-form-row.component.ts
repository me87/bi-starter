import { AfterViewInit, ChangeDetectionStrategy, Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';
import { FieldAttributes } from '../../bi-dynamic-form.model';

@Component({
  selector: 'bi-form-row',
  templateUrl: './bi-form-row.component.html',
  styleUrls: ['./bi-form-row.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BiFormRowComponent implements OnInit, AfterViewInit {

  @Input() dataSource: FieldAttributes[][] = [];

  @ContentChild('row', { static: false }) rowTemplateRef!: TemplateRef<any>;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    // console.log('AFTER', this.rowTemplateRef);
  }

}
