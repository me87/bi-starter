import { Component, OnInit } from '@angular/core';
import { BiFormFieldComponent } from '../bi-form-field/bi-form-field.component';

@Component({
  selector: 'bi-form-field-number',
  templateUrl: './bi-form-field-number.component.html',
  styleUrls: ['./bi-form-field-number.component.scss']
})
export class BiFormFieldNumberComponent extends BiFormFieldComponent {

}
