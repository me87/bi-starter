import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { DataGridConfig } from '../../bi-dynamic-form.model';
import { BiFormFieldComponent } from '../bi-form-field/bi-form-field.component';

@Component({
  selector: 'bi-form-field-simple-lookup',
  templateUrl: './bi-form-field-simple-lookup.component.html',
  styleUrls: ['./bi-form-field-simple-lookup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BiFormFieldSimpleLookupComponent extends BiFormFieldComponent implements OnChanges {

  @Input() options!: DataGridConfig;
  @Input() value!: any;

  @Output() optionsChange = new EventEmitter();

  isGridBoxOpened = false;
  gridBoxValue!: any[];

  constructor(private cd: ChangeDetectorRef) {
    super();
   }

  ngOnInit(): void {
    this.optionsChange.emit(this.config.lookupId);
  }

  ngOnChanges(changes: SimpleChanges): void {
      const options = changes?.options?.currentValue;
      const value = changes?.value?.currentValue;

      if (options && !this.gridBoxValue) {
        this.gridBoxValue = [this.formGroup.getRawValue()[this.config.name]];
      }
      if (this.options && value) {
        this.gridBoxValue = [value];
        const row = this.options.gridColumns.filter(item => item[this.config.lookupColumn] == value)[0];
        row && this.handleLookupChange(row);
      }
  }

  selectRow(row: any) {
    this.isGridBoxOpened = false;
    this.handleLookupChange(row);
    this.cd.detectChanges();
  }

}
