import { Component, OnInit } from '@angular/core';
import { BiFormFieldComponent } from '../bi-form-field/bi-form-field.component';

@Component({
  selector: 'bi-form-field-checkbox',
  templateUrl: './bi-form-field-checkbox.component.html',
  styleUrls: ['./bi-form-field-checkbox.component.scss']
})
export class BiFormFieldCheckboxComponent extends BiFormFieldComponent {

}
