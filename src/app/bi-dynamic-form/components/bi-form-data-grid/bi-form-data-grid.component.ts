import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { take } from 'rxjs/operators';
import {
  ActionType,
  DataGridConfig,
  FieldValueChangeEvent,
  FormData,
  FormDataRequest,
  FormDataRequestItem,
  GridColumnState,
} from '../../bi-dynamic-form.model';
import { BiDynamicFormService } from '../../services/bi-dynamic-form.service';
import { DataGridService } from '../../services/data-grid.service';
import { BiFormFieldComponent } from '../bi-form-field/bi-form-field.component';

@Component({
  selector: 'bi-form-data-grid',
  templateUrl: './bi-form-data-grid.component.html',
  styleUrls: ['./bi-form-data-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BiFormDataGridComponent extends BiFormFieldComponent {
  @Output() dataGridChange = new EventEmitter<any>();

  dataGridConfig!: DataGridConfig;
  selectedRow!: any;

  formData!: FormData;

  private formState!: any;

  closeFormOptions!: any;
  submitFormOptions!: any;

  visible = false;

  formEditable = false;

  actionType = ActionType;

  currentSelectedAction!: ActionType;

  gridColumnsState: any = {};

  constructor(
    private dataGridService: DataGridService,
    private cd: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit(): void {
    this.getGridConfig();

    // Definition for modal close button
    this.closeFormOptions = {
      text: 'Zatvori',
      icon: 'mdi mdi-close',
      type: 'normal',
      onClick: () => this.toggleModal(),
    };

    // Definition for modal submit button
    this.submitFormOptions = {
      text: 'Sacuvaj',
      icon: 'mdi mdi-content-save',
      type: 'success',
      stylingMode: 'contained',
      onClick: () => {
        this.currentSelectedAction === ActionType.Add
          ? this.addAction()
          : this.editAction();
        this.toggleModal();
      },
    };
  }

  selectRow(row: any) {
    this.selectedRow = row;
  }

  rowAction(actionId: ActionType) {
    this.currentSelectedAction = actionId;
    switch (this.currentSelectedAction) {
      case ActionType.Add:
        this.getFormData(actionId);
        break;
      case ActionType.Edit:
        this.getFormData(actionId);
        break;
      case ActionType.Delete:
        this.deleteAction();
        break;
      case ActionType.View:
        this.getFormData(actionId);
        break;

      default:
        break;
    }
  }

  actionIcon(actionId: ActionType) {
    switch (actionId) {
      case ActionType.Add:
        return 'mdi-plus';
      case ActionType.Delete:
        return 'mdi-delete';
      case ActionType.Edit:
        return 'mdi-pencil';
      case ActionType.View:
        return 'mdi-eye';
      default:
        return '';
    }
  }

  getGridConfig() {
    this.dataGridService
      .getDataGridConfig()
      .pipe(take(1))
      .subscribe((res: DataGridConfig) => {
        this.dataGridConfig = res;
        this.dataGridConfig.gridColumns.map((item, i) =>
          this.setGridColumnsState(item, GridColumnState.UNCHANGED, i)
        );
        // emit data grid data
        this.handleDataGridChange();
        // Force change detection
        this.cd.detectChanges();
      });
  }

  getFormData(actionId: ActionType) {
    this.dataGridService
      .getNaknadaMock()
      .pipe(take(1))
      .subscribe((res: FormData) => {
        this.formData = res;
        this.toggleModal();
        // Force change detection
        this.cd.detectChanges();
      });
  }

  formDataChange(event: FormDataRequest) {
    this.formState = this.prepareFormState(event.attributes);
  }

  toggleModal() {
    this.visible = !this.visible;
    this.cd.detectChanges();
  }

  addAction() {
    this.setGridColumnsState(
      this.formState,
      GridColumnState.ADDED,
      this.dataGridConfig.gridColumns.length
    );

    this.dataGridConfig.gridColumns = [
      ...this.dataGridConfig.gridColumns,
      this.formState,
    ];

    this.handleDataGridChange();
    this.cd.detectChanges();
  }

  editAction() {
    // update column state
    this.gridColumnsState[this.formState.ID_OBJEKAT] = {
      ...this.gridColumnsState[this.formState.ID_OBJEKAT],
      state: GridColumnState.UPDATED,
      attributes: this.prepareFormDataRequestItem(this.formState),
    };

    this.dataGridConfig.gridColumns = this.dataGridConfig.gridColumns.map(
      (item) =>
        item.ID_OBJEKAT === this.formState.ID_OBJEKAT ? this.formState : item
    );
    this.handleDataGridChange();
    this.cd.detectChanges();
  }

  deleteAction() {
    // update grid col state property with DELETED
    this.gridColumnsState[this.selectedRow.ID_OBJEKAT].state = GridColumnState.DELETED;

    this.dataGridConfig.gridColumns = this.dataGridConfig.gridColumns.filter(
      (item) => item.ID_OBJEKAT !== this.selectedRow.ID_OBJEKAT
    );

    this.handleDataGridChange();
    this.cd.detectChanges();
  }

  private prepareFormState(values: any[]) {
    return values.reduce(
      (acc: any, val: any) => ({ ...acc, [val.name]: val.value }),
      {}
    );
  }

  private handleDataGridChange() {
    const gridColumns = Object.keys(this.gridColumnsState)
      .sort(
        (a: any, b: any) =>
          this.gridColumnsState[a].index - this.gridColumnsState[b].index
      )
      .map((key) => {
        const { index, ...rest } = this.gridColumnsState[key];
        return rest;
      });

    this.dataGridChange.emit(gridColumns);
  }

  private setGridColumnsState(
    column: any,
    state: GridColumnState,
    index: number
  ) {
    this.gridColumnsState[column.ID_OBJEKAT] = {
      id: parseInt(column.ID_OBJEKAT),
      objectName: '',
      state,
      index,
      attributes: this.prepareFormDataRequestItem(column),
    };
  }

  private prepareFormDataRequestItem(data?: any): FormDataRequestItem[] {
    const form = data;
    return Object.keys(form).map((key) => ({
      name: key,
      value: form[key],
      values: null,
    }));
  }
}
