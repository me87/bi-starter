import { Component } from '@angular/core';
import { BiFormFieldComponent } from '../bi-form-field/bi-form-field.component';

@Component({
  selector: 'bi-form-field-text',
  templateUrl: './bi-form-field-text.component.html',
  styleUrls: ['./bi-form-field-text.component.scss'],
})
export class BiFormFieldTextComponent extends BiFormFieldComponent {


}
