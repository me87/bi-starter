import { Component, OnInit } from '@angular/core';
import { BiFormFieldComponent } from '../bi-form-field/bi-form-field.component';

@Component({
  selector: 'bi-form-field-textarea',
  templateUrl: './bi-form-field-textarea.component.html',
  styleUrls: ['./bi-form-field-textarea.component.scss']
})
export class BiFormFieldTextareaComponent extends BiFormFieldComponent {

}
