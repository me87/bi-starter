import { Component, OnInit } from '@angular/core';
import { BiFormFieldComponent } from '../bi-form-field/bi-form-field.component';

@Component({
  selector: 'bi-form-field-date',
  templateUrl: './bi-form-field-date.component.html',
  styleUrls: ['./bi-form-field-date.component.scss']
})
export class BiFormFieldDateComponent extends BiFormFieldComponent {

}
