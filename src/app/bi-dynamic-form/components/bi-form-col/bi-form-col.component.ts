import { ChangeDetectionStrategy, Component, ContentChild, HostBinding, Input, OnInit, TemplateRef } from '@angular/core';
import { FieldAttributes } from '../../bi-dynamic-form.model';

@Component({
  selector: 'bi-form-col',
  templateUrl: './bi-form-col.component.html',
  styleUrls: ['./bi-form-col.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BiFormColComponent implements OnInit {

  @Input() dataSource: FieldAttributes[] = [];
  @Input() index!: number;

  constructor() { }

  @HostBinding('style.min-width') minWidth!: string;
  @HostBinding('style.display') display: string = 'flex';

  @ContentChild('col', { static: false }) colTemplateRef!: TemplateRef<any>;

  ngOnInit(): void {

  }

  isSeparator(fields: FieldAttributes[]): boolean {
    // console.log('fields ', fields.map(item => item.width));
    return !!fields.reduce((acc, value) => acc + value.width,0);
  }

  colWidth(width: number, arr: any[], index?: number) {
    const fields = arr.map((field: FieldAttributes) => {
      // check if label width is bigger then field width, if so assign label width to field width
      const labelLength = field.label.length * 5; // number 5 is a character width, current character width based on font size, adjustable
      field.width = field.width > labelLength ? field.width : labelLength;
      return field;
    });

    const total = fields.reduce((acc, value) => acc + value.width, 0);
    return (width/total) * 100;
  }

}
