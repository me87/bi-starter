import { Pipe, PipeTransform } from '@angular/core';
import { FieldAttributes } from './bi-dynamic-form.model';

@Pipe({
  name: 'biFormMapper'
})
export class BiFormMapperPipe implements PipeTransform {

  transform(value: FieldAttributes[], removeHiddenFields = true): FieldAttributes[][][] {
    // Convert boolean type fields value from string to boolean
    const fields = value.map(item => {
      if (item.type === 'bool' && item.value !== null) {
        item.value = item.value.toString() === 'true'
      }
      return item;
    })

    // Filter out hidden fields
    const onlyVisibleFields = fields.filter(item => item.hidden !== '1');

    // Group fields by headerId (Make Fieldsets)
    const rows = this.groupBy(removeHiddenFields ? onlyVisibleFields : fields, 'headerId');

    // Group fields in header into rows
    return rows.map((item: any[]) => this.groupBy(item, 'row'));
  }

  private groupBy(array: any[], groupBy: string) : any[] {
    const groups = array.reduce(
      (groups: any, field) => ({
        ...groups,
        [field[groupBy]]: [...(groups[field[groupBy]] || []), field],
      }),
      {}
    );
    return Object.values(groups);
  }

}
