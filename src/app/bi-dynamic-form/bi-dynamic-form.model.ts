export enum FieldType {
  Text = 'text',
  Textarea = 'textarea',
  Number = 'number',
  Date = 'date',
  Radio = 'radio',
  Checkbox = 'checkbox',
  Dropdown = 'dropdown',
  Grid = 'grid',
  Search = 'search'
}

export enum ActionType {
  Add = 95,
  Delete = 96,
  Edit = 97,
  View = 103
}

export enum GridColumnState {
  UNCHANGED,
  INSERTED,
  UPDATED,
  DELETED,
  ADDED
}

export interface FormDataResponse {
  data: FormData;
  success: boolean;
  errorMessage: any;
  errorMessages: any[];
}
export interface FormData {
  attributes: FieldAttributes[];
  asIsAttributes?: any[];
  objectId?: number;
  behaviorName: string;
  objectName?: any;
  dmtExistOnBehavior?: boolean;
  attributesThatLookupsDependsOn?: any;
  attributesThatDecisionDependsOn?: any;
  objectData?: any;
  attributesThatDepandsOnDecision?: any[];
  attributesThatCalculatedValuesDependsOn?: any[];
  attributesWithCalculatedValue?: any[];
}

export interface FieldAttributes {
  name: string;
  label: string;
  mask: string;
  width: number;
  height: number;
  left: number;
  key: boolean;
  defaultValue: string;
  requered: string;
  type: string;
  order: number;
  row: number;
  masterRow: string;
  value: any;
  hidden: any;
  readOnly: any;
  multiLine: boolean;
  write: any;
  log: any;
  lookupTable: string;
  lookupId: number;
  useCach: boolean;
  lookupColumn: string;
  lookupF3: boolean;
  mySimpleLookup: boolean;
  mySearchCriteriaNeeded: boolean;
  regExpresion: string;
  radioButtonGroup: number;
  emptyHidden: boolean;
  onLeave: any;
  lookupType: any;
  where: any;
  calculatedValue: string;
  headerId: number;
  validateStoredProcedure: any;
  validateDataWidth: any;
  listType: any;
  groupByField?: string;
  radioGroupId?: number;
}

export interface FormDataRequest {
  id?: number;
  objectName?: string;
  roleId?: number;
  userId?: number;
  oj?: string;
  counter?: number;
  action?: number;
  version_object?: number;
  questionGuid?: string;
  attributes: FormDataRequestItem[];
  isFormValid?: boolean;
}
export interface FormDataRequestItem {
  name: string;
  value: any;
  values?: any;
}
export interface DynamicFormFields {
  [key: number]: FieldAttributes[];
}

export interface DataGridConfigResponse {
  data: DataGridConfig;
}

export interface DataGridConfig {
    visibleColumns: string[];
    gridColumns: any[];
    buttons: DataGridActionButton[];
}

export interface DataGridActionButton {
  label: string;
  actionId: number;
  alwaysEnabled: boolean;
}

export interface FieldValueChangeEvent {
  fieldName: string;
  oldValue: any;
  newValue: any;
  form?: FormDataRequest;
}

export interface DecisionRequest {
  behaviorName: string;
  decisionGuid: string;
  inputs: {[key: string]: any}
}

export interface DecisionResponse {
  decisionResults: DecisionResult[]
}

export interface DecisionResult {
  attributeName: string;
  readOnly?: boolean;
  value?: any;
  required?: boolean;
  hidden?: boolean;
}
