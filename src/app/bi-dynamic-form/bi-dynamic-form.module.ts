import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BiDynamicFormComponent } from './bi-dynamic-form.component';
import { DxAccordionModule, DxButtonModule, DxCheckBoxModule, DxDataGridModule, DxDateBoxModule, DxDropDownBoxModule, DxNumberBoxModule, DxPopupModule, DxScrollViewModule, DxTextAreaModule, DxTextBoxModule, DxValidatorModule } from 'devextreme-angular';
import { BiFormMapperPipe } from './bi-form-mapper.pipe';
import { BiFormGroupComponent } from './components/bi-form-group/bi-form-group.component';
import { BiFormRowComponent } from './components/bi-form-row/bi-form-row.component';
import { BiFormColComponent } from './components/bi-form-col/bi-form-col.component';
import { BiFormFieldComponent } from './components/bi-form-field/bi-form-field.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BiFormDataGridComponent } from './components/bi-form-data-grid/bi-form-data-grid.component';
import { BiFormFieldTextComponent } from './components/bi-form-field-text/bi-form-field-text.component';
import { BiFormFieldNumberComponent } from './components/bi-form-field-number/bi-form-field-number.component';
import { BiFormFieldTextareaComponent } from './components/bi-form-field-textarea/bi-form-field-textarea.component';
import { BiFormFieldDateComponent } from './components/bi-form-field-date/bi-form-field-date.component';
import { BiFormFieldCheckboxComponent } from './components/bi-form-field-checkbox/bi-form-field-checkbox.component';
import { BiFormFieldRadioComponent } from './components/bi-form-field-radio/bi-form-field-radio.component';
import { BiFormLookupComponent } from './components/bi-form-lookup/bi-form-lookup.component';
import { BiFormFieldSimpleLookupComponent } from './components/bi-form-field-simple-lookup/bi-form-field-simple-lookup.component';
import { ShowColDirective } from './directives/show-col.directive';
import { BiFormFieldSearchLookupComponent } from './components/bi-form-field-search-lookup/bi-form-field-search-lookup.component';



@NgModule({
  declarations: [
    BiDynamicFormComponent,
    BiFormMapperPipe,
    BiFormGroupComponent,
    BiFormRowComponent,
    BiFormColComponent,
    BiFormFieldComponent,
    BiFormDataGridComponent,
    BiFormFieldTextComponent,
    BiFormFieldNumberComponent,
    BiFormFieldTextareaComponent,
    BiFormFieldDateComponent,
    BiFormFieldCheckboxComponent,
    BiFormFieldRadioComponent,
    BiFormLookupComponent,
    BiFormFieldSimpleLookupComponent,
    ShowColDirective,
    BiFormFieldSearchLookupComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DxTextBoxModule,
    DxTextAreaModule,
    DxNumberBoxModule,
    DxCheckBoxModule,
    DxDateBoxModule,
    DxDropDownBoxModule,
    DxValidatorModule,
    DxButtonModule,
    DxAccordionModule,
    DxDataGridModule,
    DxPopupModule,
    DxScrollViewModule,
  ],
  exports: [
    BiDynamicFormComponent,
  ]
})
export class BiDynamicFormModule { }
