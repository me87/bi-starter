import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { filter, map, pairwise, startWith } from 'rxjs/operators';
import {
  DataGridConfig,
  DecisionRequest,
  DecisionResult,
  FieldAttributes,
  FieldType,
  FieldValueChangeEvent,
  FormData,
  FormDataRequest,
  FormDataRequestItem,
} from './bi-dynamic-form.model';
import { BiDynamicFormService } from './services/bi-dynamic-form.service';
@Component({
  selector: 'bi-dynamic-form',
  templateUrl: './bi-dynamic-form.component.html',
  styleUrls: ['./bi-dynamic-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BiDynamicFormComponent implements OnInit {
  @Input() formData!: FormData;
  @Output() formDataChange = new EventEmitter<FormDataRequest>();

  form!: FormGroup;

  simpleLookups: { [key: number]: DataGridConfig } = {};
  searchLookups: { [key: number]: DataGridConfig } = {};
  attributesThatDecisionDependsOn: { [key:string]: { guid: string, fields: string[]}[]} = {};

  // Map of dependent fields based on same lookupId
  private lookupDependentFields!: { [key: number]: FieldAttributes[] };

  // Map of data grid fields values
  private dataGridFieldsValues!: { [key: string]: any[] };


  // Field template references
  @ViewChild('textField', { static: true })
  textField!: TemplateRef<HTMLElement>;
  @ViewChild('textAreaField', { static: true })
  textAreaField!: TemplateRef<HTMLElement>;
  @ViewChild('numberField', { static: true })
  numberField!: TemplateRef<HTMLElement>;
  @ViewChild('dateField', { static: true })
  dateField!: TemplateRef<HTMLElement>;
  @ViewChild('checkboxField', { static: true })
  checkboxField!: TemplateRef<HTMLElement>;
  @ViewChild('radioField', { static: true })
  radioField!: TemplateRef<HTMLElement>;
  @ViewChild('headerField', { static: true })
  headerField!: TemplateRef<HTMLElement>;
  @ViewChild('dataGridField', { static: true })
  dataGridField!: TemplateRef<HTMLElement>;
  @ViewChild('simpleLookupField', { static: true })
  simpleLookupField!: TemplateRef<HTMLElement>;
  @ViewChild('searchLookupField', { static: true })
  searchLookupField!: TemplateRef<HTMLElement>;

  get formDataAttributes(): FieldAttributes[] {
    return this.formData.attributes;
  }

  get formDataAttributesThatDecisionDependsOn(): { [key: string]: string[] } {
    return this.formData.attributesThatDecisionDependsOn;
  }

  get formDataAttributesThatLookupsDependsOn() {
    return this.formData.attributesThatLookupsDependsOn;
  }

  get formValue() {
    return this.form.getRawValue();
  }

  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private biDynamicFormService: BiDynamicFormService
  ) {}

  ngOnInit(): void {
    // Initialize form
    this.initForm();

    // Emit first form value
    this.handleFormDataChange();

    // Tracking value changes of internal form
    this.trackFormValueChanges();

    // Create a map of dependent fields based on same lookupId
    this.setLookupDependentFields();

    // Create a map of dependent fields based on decision
    this.setAttributesThatDecisionDependsOn();

  }

  // SHOW/HIDE LAYOUT COLUMNS AND FIELDS
  showCol(col: FieldAttributes) {
    const form = this.formValue;

    if (col.emptyHidden && !form[col.name]) return false;

    if (col.hidden === '0') return true;

    const { hidden } = col;

    const hiddenValues =
      hidden && hidden.split('AND').map((item: string) => item.trim());

    const values = hiddenValues.map((item: string) => {
      const isNegation = !!item.search('!');

      const refField = item.split('!')[1] || item;

      const field =
        this.formDataAttributes.find((item) => item.name === refField) || '';

      const fieldValue = form[field && field.name];

      return isNegation ? !fieldValue : fieldValue;
    });

    return values.every(Boolean);

    // const isNegation = !!hidden.search('!');

    // const refField = hidden.split('!')[1] || hidden;

    // const field =
    //   this.formDataAttributes.find((item) => item.name === refField) || '';

    // const fieldValue = form[field && field.name];

    // return isNegation ? !fieldValue : fieldValue;
  }

  // RENDER FIELDS BASED ON FIELD TYPE
  inputType(field: FieldAttributes) {
    const type = this.biDynamicFormService.fieldType(field);

    switch (type) {
      case FieldType.Checkbox:
        return this.checkboxField;
      case FieldType.Radio:
        return this.radioField;
      case FieldType.Number:
        return this.numberField;
      case FieldType.Date:
        return this.dateField;
      case FieldType.Grid:
        return this.dataGridField;
      case FieldType.Textarea:
        return this.textAreaField;
      case FieldType.Dropdown:
        return this.simpleLookupField;
      case FieldType.Search:
        return this.searchLookupField;
      default:
        return this.textField;
    }
  }

  handleLookup(selectedValue: any, field: FieldAttributes) {
    // Select dependent fields by lookupId
    const fieldsToUpdate = this.lookupDependentFields[field.lookupId];
    // Update dependent fields based on lookup selection
    fieldsToUpdate.forEach((item) => {
      this.patchFormFieldValue(item.name, selectedValue[item.lookupColumn]);
    });
  }

  handleModelUpdateForRadioButtons(field: FieldAttributes) {
    const { name, radioButtonGroup } = field;
    const fields = this.formDataAttributes.filter(
      (item) => item.radioButtonGroup === radioButtonGroup
    );

    fields.forEach((item) => {
      item.name === name ? (item.value = true) : (item.value = false);
      this.patchFormFieldValue(item.name, item.value);
    });
  }

  // GET OPTIONS FOR SIMPLE LOOKUP
  handleSimpleLookupOptions(lookupId: number, attributeName: string) {
    const attributesThatLookupDependsOn =
      this.biDynamicFormService.keyValueMapper(
        this.formDataAttributesThatLookupsDependsOn[lookupId],
        this.formValue
      );

    const req = { lookupId, attributeName, attributesThatLookupDependsOn };

    this.biDynamicFormService.loadSimpleLookupOptions(req, (res) => {
      this.simpleLookups = { ...this.simpleLookups, [lookupId]: res };
      // Force change detection
      this.cd.detectChanges();
    });
  }

  // GET OPTIONS FOR SEARCH LOOKUP
  handleSearchLookupOptions(searchText: string, lookupId: number, attributeName: string) {
    const attributesThatLookupDependsOn = this.biDynamicFormService.keyValueMapper(
      this.formDataAttributesThatLookupsDependsOn[lookupId],
      this.formValue
    );

    const req = { lookupId, attributeName, searchText, attributesThatLookupDependsOn };

    this.biDynamicFormService.loadSearchLookupOptions(
      req,
      (res) => {
        this.searchLookups = { ...this.searchLookups, [lookupId]: res };
        // Force change detection
        this.cd.detectChanges();
      }
    );
  }

  handleCalculatedValue(fieldName: string) {

    if (!this.attributesThatDecisionDependsOn[fieldName]) return;

    const requests = this.attributesThatDecisionDependsOn[fieldName].map(item => {
      const inputs = this.biDynamicFormService.keyValueMapper(
        item.fields,
        this.formValue
      );

      const req: DecisionRequest = {
        behaviorName: this.formData.behaviorName,
        decisionGuid: item.guid,
        inputs,
      };

      return req;
    })

    this.biDynamicFormService.loadCalculatedValuesConcat(requests, (data: any) => {
      this.patchCalculatedValues(data);
      // Force change detection
      this.cd.detectChanges();
    });
  }

  handleDataGridChange(event: any, fieldName: string) {
    this.dataGridFieldsValues = {
      ...this.dataGridFieldsValues,
      [fieldName]: event,
    };
    console.log('DATA GRID DATA: ', this.dataGridFieldsValues);
    this.handleFormDataChange();
  }

  private handleFormDataChange() {
    const form: FormDataRequest = {
      isFormValid: this.form.valid,
      id: this.formData.objectId,
      attributes: this.prepareFormDataRequestItem(),
    };

    this.formDataChange.emit(form);
  }

  private initForm() {
    this.form = this.createForm(this.formDataAttributes);
  }

  createForm(fields: FieldAttributes[]) {
    // initialize the form
    const group = this.fb.group({});
    // Loop through formData(config) and build the form
    fields.forEach((control) =>
      group.addControl(
        control.name,
        this.fb.control(
          {
            value: this.biDynamicFormService.prepareFormValues(control),
            disabled: control.readOnly == '1',
          },
          control.requered == '1' ? Validators.required : null
        )
      )
    );
    return group;
  }

  private trackFormValueChanges() {
    this.form.valueChanges
      .pipe(
        startWith(this.formValue),
        pairwise(),
        map(([oldValues, newValues]) =>
          this.prepareFieldValueChangeEvent(oldValues, newValues)
        ),
        filter((val) => !!val)
      )
      .subscribe((val) => {
        console.log('FORM FIELD CHAGE EVENT (SENT): ', val);
        if (val) {
          // check if there are calculated value fields
          this.handleCalculatedValue(val.fieldName);
          // emit changed form field
          this.handleFormDataChange();
          // this.formDataChange.emit(val.form);
        }
      });
  }

  private patchFormFieldValue(fieldName: string, value: any) {
    const form = this.form.value;

    if (form[fieldName] !== value) {
      this.form.get(fieldName)?.patchValue(value);
    }
  }

  private setLookupDependentFields() {
    this.lookupDependentFields = this.formDataAttributes.reduce(
      (acc, value, i, arr) => ({
        ...acc,
        [value.lookupId]: arr.filter(
          (item) => item.lookupId === value.lookupId
        ),
      }),
      {}
    );
  }

  private prepareFieldValueChangeEvent(oldValues: any, newValues: any) {
    // find changed field name by comparing values
    const fieldName = Object.keys(newValues).find(
      (k) => newValues[k] != oldValues[k]
    );

    // if no value is changed return null
    if (!fieldName) return null;

    const change: FieldValueChangeEvent = {
      fieldName,
      oldValue: oldValues[fieldName],
      newValue: newValues[fieldName],
      form: {
        id: this.formData.objectId,
        attributes: this.prepareFormDataRequestItem(),
      },
    };

    return change;
  }

  private prepareFormDataRequestItem(data?: any): FormDataRequestItem[] {
    const form = data || this.form.getRawValue();
    return Object.keys(form).map((key) => ({
      name: key,
      value: form[key],
      values: this.prepareFormDataRequestItemValues(key),
    }));
  }

  private prepareFormDataRequestItemValues(key: string) {
    const attributes = this.biDynamicFormService.getFieldAttributeByName(
      key,
      this.formDataAttributes
    );

    if (attributes?.type !== 'list') {
      return null;
    }

    const values =
      this.dataGridFieldsValues && this.dataGridFieldsValues[attributes.name];

    return values;
  }

  patchCalculatedValues(data: DecisionResult[]) {
    data.map((item: any) => {
      let { attributeName, value, readOnly, required, hidden } = item;

      Object.keys(item).map((key: any) => {
        this.formData.attributes.forEach((attribute: any) => {
          if (attribute.name === attributeName) {
            attribute[key] = item[key];
          }
          return attribute;
        });
      });

      // this.formData.attributes = [...this.formData.attributes]
      if (!!readOnly) {
        readOnly == '1'
          ? this.form.get(attributeName)?.disable()
          : this.form.get(attributeName)?.enable();
      }

      if (!!required) {
        required == '1'
          ? this.form.get(attributeName)?.setValidators([Validators.required])
          : this.form.get(attributeName)?.setValidators([]);
      }

      if (!value) return;

      if (this.isBoolean(attributeName)) {
        value = value == 'true' || value == '1';
      }

      this.patchFormFieldValue(attributeName, value)

    });
  }

  private setAttributesThatDecisionDependsOn() {
    const fieldNames = this.formDataAttributes.map(item => item.name);
    this.attributesThatDecisionDependsOn = fieldNames.reduce((acc: any, fieldName: string) => {

      Object.keys(this.formDataAttributesThatDecisionDependsOn).map(key => {
        if (this.formDataAttributesThatDecisionDependsOn[key].includes(fieldName)) {
          acc[fieldName] = [...acc[fieldName] || [], { guid: key, fields: this.formDataAttributesThatDecisionDependsOn[key]}]
        }
      })

      return acc
    }, {})
  }

  private isBoolean(attributeName: string) {
    const isBool = this.biDynamicFormService.getFieldAttributeByName(attributeName, this.formDataAttributes)?.type == 'bool';
    return isBool;
  }
}
