import {
  Directive,
  ElementRef,
  Input,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldAttributes } from '../bi-dynamic-form.model';

export interface ShowColumnModel {
  col: FieldAttributes;
  rowGroup: FieldAttributes[];
  formGroup: FormGroup
}

@Directive({
  selector: '[showCol]',
})
export class ShowColDirective {
  constructor(
    private element: ElementRef,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {}

  @Input()
  set showCol(val: ShowColumnModel) {
    if(this.show(val)) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

  show(val: ShowColumnModel) {
    const { hidden } = val?.col;

    if (hidden === '0') return true;

    const form = val?.formGroup.value;

    const isNegation = !!hidden.search('!');

    const refField = hidden.split('!')[1] || hidden;

    const field = val?.rowGroup.find((item) => item.name === refField) || '';

    const fieldValue = form[field && field.name];

    return isNegation ? !fieldValue : fieldValue;
  }
}
