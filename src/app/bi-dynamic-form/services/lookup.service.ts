import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataGridConfig, DataGridConfigResponse } from '../bi-dynamic-form.model';

@Injectable({
  providedIn: 'root'
})
export class LookupService {

  constructor(private httpClient: HttpClient) { }

  getLookup(req: any): Observable<DataGridConfig> {
    return this.httpClient
      .get<DataGridConfigResponse>(`/assets/mock/lookup.mock.json`)
      .pipe(map((res) => res.data));
  }

  getSimpleLookup(req: any): Observable<DataGridConfig> {
    return this.httpClient
      .get<DataGridConfigResponse>(`/assets/mock/lookup.2808.mock.json`)
      .pipe(map((res) => res.data));
  }
}
