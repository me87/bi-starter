import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataGridConfig, DataGridConfigResponse, FormDataResponse, FormData } from '../bi-dynamic-form.model';

@Injectable({
  providedIn: 'root'
})
export class DataGridService {

  constructor(private httpClient: HttpClient) { }

  getDataGridConfig(): Observable<DataGridConfig> {
    return this.httpClient
      .get<DataGridConfigResponse>('/assets/mock/dataGrid.mock.json')
      .pipe(map((res) => res.data));
  }

  getDefaultMock(): Observable<FormData> {
    return this.httpClient
      .get<FormDataResponse>('/assets/mock/default.mock.json')
      .pipe(map((res) => res.data));
  }

  getNaknadaMock(): Observable<FormData> {
    return this.httpClient
      .get<FormDataResponse>('/assets/mock/NAKNADA_EDIT.json')
      .pipe(map((res) => res.data));
  }
}
