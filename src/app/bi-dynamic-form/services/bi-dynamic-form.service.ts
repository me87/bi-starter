import { Injectable } from '@angular/core';
import { concat, from } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import {
  DataGridConfig,
  DecisionRequest,
  DecisionResult,
  FieldAttributes,
  FieldType,
} from '../bi-dynamic-form.model';
import { CalculatedValuesService } from './calculated-values.service';
import { LookupService } from './lookup.service';

@Injectable({
  providedIn: 'root',
})
export class BiDynamicFormService {
  constructor(
    private calculatedValuesService: CalculatedValuesService,
    private lookupService: LookupService
  ) {}

  loadCalculatedValuesConcat(
    req: DecisionRequest[],
    callback: (res: any) => void
  ) {
    const request = req.map((item) =>
      this.calculatedValuesService.getCalculatedValues(item).pipe(
        take(1),
        map((res: DecisionResult[]) => {
          callback && callback(res);
        })
      )
    );

    from([...request])
      .pipe(concatMap((res) => res))
      .subscribe();
  }

  loadCalculatedValues(req: DecisionRequest, callback: (res: any) => void) {
    this.calculatedValuesService
      .getCalculatedValues(req)
      .pipe(take(1))
      .subscribe((res: DecisionResult[]) => callback && callback(res));
  }

  loadSimpleLookupOptions(req: any, callback: (res: any) => void) {
    this.lookupService
      .getSimpleLookup(req)
      .pipe(take(1))
      .subscribe((res: DataGridConfig) => callback && callback(res));
  }

  loadSearchLookupOptions(req: any, callback: (res: any) => void) {
    this.lookupService
      .getLookup(req)
      .pipe(take(1))
      .subscribe((res: DataGridConfig) => callback && callback(res));
  }

  fieldType(field: FieldAttributes) {
    const {
      type,
      multiLine,
      radioButtonGroup,
      mySimpleLookup,
      lookupF3,
      mySearchCriteriaNeeded,
    } = field;
    if (
      (type === 'int' && lookupF3 && !mySearchCriteriaNeeded) ||
      (type === 'string' && lookupF3 && !mySearchCriteriaNeeded)
    )
      return FieldType.Dropdown;
    if (
      (type === 'int' && lookupF3 && mySearchCriteriaNeeded) ||
      (type === 'string' && lookupF3 && mySearchCriteriaNeeded)
    )
      return FieldType.Search;
    if (type === 'bool' && radioButtonGroup == 0) return FieldType.Checkbox;
    if (type === 'bool' && radioButtonGroup > 0) return FieldType.Radio;
    if (type === 'int' && !mySimpleLookup) return FieldType.Number;
    if (type === 'datetime') return FieldType.Date;
    if (type === 'list') return FieldType.Grid;
    if (type === 'string' && multiLine) return FieldType.Textarea;
    return FieldType.Text;
  }

  extractGuid(value: string): string {
    return value ? value.split(':').pop()?.split('|')[0] ?? '' : '';
  }

  keyValueMapper(fields: string[], form: any) {
    return (
      fields && fields.reduce((acc, val) => ({ ...acc, [val]: form[val] }), {})
    );
  }

  getFieldAttributeByName(fieldName: string, attributes: FieldAttributes[]) {
    return attributes.find((item) => item.name === fieldName);
  }

  prepareFormValues(field: FieldAttributes) {
    // Convert boolean type fields value from string to boolean
    if (field.type === 'bool' && field.value !== null) {
      field.value =
        field.value.toString() === 'true' || field.value.toString() === '1';
    }
    if (field.type === 'datetime' && field.value !== null) {
      field.value = field.value.trim() === '' ? null : field.value;
    }
    return field.value;
  }
}
