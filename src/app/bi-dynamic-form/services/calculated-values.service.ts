import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DecisionRequest, DecisionResult } from '../bi-dynamic-form.model';

@Injectable({
  providedIn: 'root'
})
export class CalculatedValuesService {

  constructor(private httpClient: HttpClient) { }

  getCalculatedValues(body: DecisionRequest): Observable<DecisionResult[]> {
    return this.httpClient
      .get<any>(`/assets/mock/decision.mock.json`)
      .pipe(map((res) => res.data && res.data.decisionResults));
  }

}
