export const navigation = [
  {
    text: 'Klijenti',
    icon: 'mdi mdi-account-group-outline',
    path: '/profile'
  },
  {
    text: 'Front Office',
    icon: 'mdi mdi-account-tie',
    items: [
      {
        text: 'Zahtev savetnika',
        path: '/tasks'
      },
      {
        text: 'Izmena racuna',
        path: '',
      },
      {
        text: 'Zahtevi kreditnih specijalista',
      },
      {
        text: 'Zahtevi regionalni direktor',
      },
      {
        text: 'Zahtevi regionalni direktor retail',
      },
      {
        text: 'Rezultati automatske analize',
      },
      {
        text: 'Rezultati knockout kriterijuma',
      },
      {
        text: 'Visi blagajnik',
      },
      {
        text: 'Zahtevi savetnika FL',
      },
      {
        text: 'BM Analiza FL',
      },
      {
        text: 'Zahtevi AML',
      },
      {
        text: 'RBP Calculator (SB)',
      },
      {
        text: 'RBP Calculator (SME/LC)',
      },
      {
        text: 'Pregled zahteva FL',
      },
      {
        text: 'Kupoprodaja - Diler',
      },
      {
        text: 'Kupoprodaja EXP',
      },
    ]
  },
  {
    text: 'Kreditna Analiza',
    icon: 'mdi mdi-finance',
    items: [
      {
        text: 'Zahtev savetnika',
        path: ''
      },
    ]
  },
  {
    text: 'Middle Office',
    icon: 'mdi mdi-folder-swap-outline'
  },
  {
    text: 'Back Office',
    icon: 'mdi mdi-desktop-tower-monitor'
  },
  {
    text: 'Dokumentarni poslovi',
    icon: 'mdi mdi-file-document-multiple-outline'
  },
  {
    text: 'COLMS',
    icon: 'mdi mdi-umbrella-outline'
  },
  {
    text: 'Proizvodi',
    icon: 'mdi mdi-cube-outline'
  },
  {
    text: 'Administracija',
    icon: 'mdi mdi-application-settings'
  },
  {
    text: 'Izvestaji',
    icon: 'mdi mdi-file-chart-outline'
  },
  {
    text: 'Servis',
    icon: 'mdi mdi-room-service-outline'
  },
  {
    text: 'Workflow Test',
    icon: 'mdi mdi-bullseye'
  },
];
