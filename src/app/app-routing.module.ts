import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  LoginFormComponent,
  ResetPasswordFormComponent,
  CreateAccountFormComponent,
  ChangePasswordFormComponent,
} from './shared/components';
import { AuthGuardService } from './shared/services';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { TasksComponent } from './pages/tasks/tasks.component';
import {
  DxAccordionModule,
  DxAutocompleteModule,
  DxButtonModule,
  DxDataGridModule,
  DxFormModule,
  DxNumberBoxModule,
  DxPopupModule,
  DxRadioGroupModule,
  DxScrollViewModule,
  DxTagBoxModule,
  DxTextAreaModule,
  DxTextBoxModule,
  DxTreeListModule,
} from 'devextreme-angular';
import { CommonModule } from '@angular/common';
import { BiDynamicFormModule } from './bi-dynamic-form/bi-dynamic-form.module';
import { DynamicFormFieldGroupsExampleComponent } from './pages/dynamic-form-field-groups-example/dynamic-form-field-groups-example.component';
import { DynamicFormListTypeExampleComponent } from './pages/dynamic-form-list-type-example/dynamic-form-list-type-example.component';
import { DynamicFormBasicExampleComponent } from './pages/dynamic-form-basic-example/dynamic-form-basic-example.component';

const routes: Routes = [
  {
    path: 'tasks',
    component: TasksComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'login-form',
    component: LoginFormComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'reset-password',
    component: ResetPasswordFormComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'create-account',
    component: CreateAccountFormComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'change-password/:recoveryCode',
    component: ChangePasswordFormComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: '**',
    redirectTo: 'home',
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { useHash: true }),
    DxDataGridModule,
    DxFormModule,
    DxAutocompleteModule,
    DxButtonModule,
    DxAccordionModule,
    DxTagBoxModule,
    DxRadioGroupModule,
    DxPopupModule,
    DxScrollViewModule,
    DxTextAreaModule,
    DxTextBoxModule,
    DxNumberBoxModule,
    DxTreeListModule,
    BiDynamicFormModule,
  ],
  providers: [AuthGuardService],
  exports: [RouterModule],
  declarations: [
    HomeComponent,
    ProfileComponent,
    TasksComponent,
    DynamicFormFieldGroupsExampleComponent,
    DynamicFormListTypeExampleComponent,
    DynamicFormBasicExampleComponent,
  ],
})
export class AppRoutingModule {}
